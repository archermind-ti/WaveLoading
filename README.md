# WaveLoading


### 项目介绍

A wave-like loading,This library provides a wave loading animation as a Component

### 调用差异

目前Ohos不支持继承Element实现自定义多样式Element，所以修改为自定义的Component组件，设置参数后调用draw()方法重绘。调用方式略有不同

### 效果演示

<img src="/picture/waveloading.gif" width="33%" />


###  安装教程

方式一：

1. 下载模块代码添加到自己的工程

2. 关联使用

   ```
   dependencies {
       implementation fileTree(dir: 'libs', include: ['*.jar', '*.har'])
       implementation project(':library')
       testImplementation 'junit:junit:4.13'
   	……
   }
   ```

4. gradle sync

方式二：

```
allprojects {
    repositories {
        mavenCentral()        
    }
}

...
dependencies {
	...
	implementation 'com.gitee.archermind-ti:WaveLoading:1.0.1'
	...
}
```

### 使用说明

按如下方式使用：

```
//资源图片为底
WaveLoadingView waveLoading = (WaveLoadingView)findComponentById(ResourceTable.Id_image);
waveLoading.setWaveElement(this, ResourceTable.Media_chrome_logo);
waveLoading.draw();
```

或者：

```
//纯色底
waveLoading.setWaveColor(Color.getIntColor("#FF4081"));
waveLoading.draw();
```

其他可配置APIs：

- `public void setWaveAmplitude(int amplitude)`, 设置波幅
- `public void setWaveLength(int length)`, 设置波长
- `public void setWaveSpeed(int step)`, 设置波移动速度
- `public void setLevel(int level)`, 设置水位
- `public void setIndeterminate(boolean indeterminate)`, 设置运行模式，如果以“不确定”模式运行，它将一遍又一遍地增加水位，否则以固定水位加载.
- `public void setIndeterminateAnimator(ValueAnimator animator)`, 设置您自定义的动画制作器，以不确定的模式加载波浪动画.




###  版本迭代

- v1.0.1

## License

MIT

