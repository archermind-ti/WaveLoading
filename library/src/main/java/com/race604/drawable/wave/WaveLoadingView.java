/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.race604.drawable.wave;

import ohos.agp.animation.Animator;
import ohos.agp.animation.AnimatorValue;
import ohos.agp.components.AttrSet;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.element.Element;
import ohos.agp.components.element.PixelMapElement;
import ohos.agp.render.*;
import ohos.agp.utils.*;
import ohos.app.Context;
import ohos.app.dispatcher.TaskDispatcher;
import ohos.global.resource.Resource;
import ohos.hiviewdfx.HiLog;
import ohos.hiviewdfx.HiLogLabel;
import ohos.media.image.ImageSource;
import ohos.media.image.PixelMap;
import ohos.media.image.common.PixelFormat;
import ohos.media.image.common.Size;

public class WaveLoadingView extends Component implements Animatable, Component.DrawTask  {
    static final HiLogLabel TAG = new HiLogLabel(HiLog.LOG_APP, 0, "WaveLoadingView");

    private static final float WAVE_HEIGHT_FACTOR = 0.2f;
    private static final float WAVE_SPEED_FACTOR = 0.02f;
    private static final int UNDEFINED_VALUE = Integer.MIN_VALUE;
    private Element mDrawable;
    private int mWidth, mHeight;
    private int mWaveHeight = UNDEFINED_VALUE;
    private int mWaveLength = UNDEFINED_VALUE;
    private int mWaveStep = UNDEFINED_VALUE;
    private int mWaveOffset = 0;
    private int mWaveLevel = 0;
    private AnimatorValue mAnimator = null;
    private float mProgress = 0.3f;
    private Paint mPaint;
    private PixelMap mMask;
    private Matrix mMatrix = new Matrix();
    private boolean mRunning = false;
    private boolean mIndeterminate = false;

    private TaskDispatcher dispatcher = getContext().getUITaskDispatcher();

    private BlendMode sXfermode = BlendMode.DST_IN;
    private static ColorMatrix sGrayFilter = new ColorMatrix(new float[]{
            0.264F, 0.472F, 0.088F, 0, 0,
            0.264F, 0.472F, 0.088F, 0, 0,
            0.264F, 0.472F, 0.088F, 0, 0,
            0,      0,      0,      1, 0
    });
    private ColorMatrix mCurFilter = null;

    public WaveLoadingView(Context context) {
        super(context);
    }

    public WaveLoadingView(Context context, AttrSet attrSet) {
        super(context, attrSet);
    }

    public void setWaveColor(int color) {
        PixelMap.InitializationOptions options = new PixelMap.InitializationOptions();
        options.pixelFormat = PixelFormat.ARGB_8888;
        options.editable = true;
        options.size = new Size(getComponentSize().getSizeXToInt(), getComponentSize().getSizeYToInt());
        PixelMap pixelMap = PixelMap.create(options);
        pixelMap.writePixels(color);
        PixelMapElement element = new PixelMapElement(pixelMap);

        init(element);
    }

    public void setWaveElement(Context context, int imgRes) {
        try {
            Resource resource = context.getResourceManager().getResource(imgRes);
            ImageSource imageSource = ImageSource.create(resource, null);
            ImageSource.DecodingOptions decodingOpts = new ImageSource.DecodingOptions();
            PixelMap pixelMap = imageSource.createPixelmap(decodingOpts);
            PixelMapElement element = new PixelMapElement(pixelMap);

            init(element);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private void init(Element drawable) {
        mDrawable = drawable;
        mMatrix.reset();
        mPaint = new Paint();
        mPaint.setFilterBitmap(false);
        mPaint.setColor(Color.BLACK);
        mPaint.setBlendMode(sXfermode);

        mWidth = mDrawable.getWidth();
        mHeight = mDrawable.getHeight();

        if (getWidth() == 0 && getHeight() == 0) {
            setLayoutConfig(new ComponentContainer.LayoutConfig(mWidth, mHeight));
            setBounds(0, 0, mWidth, mHeight);
        }

        if (mWidth > 0 && mHeight > 0) {
            mWaveLength = mWidth;
            mWaveHeight = Math.max(8, (int) (mHeight * WAVE_HEIGHT_FACTOR));
            mWaveStep = Math.max(1, (int) (mWidth * WAVE_SPEED_FACTOR));
            updateMask(mWidth, mWaveLength, mWaveHeight);
        }

        setProgress(0);
        start();
    }

    /**
     * Set wave move distance (in pixels) in very animation frame
     * @param step distance in pixels
     */
    public void setWaveSpeed(int step) {
        mWaveStep = Math.min(step, mWidth / 2);
    }

        /**
     * Set wave amplitude (in pixels)
     * @param amplitude
     */
    public void setWaveAmplitude(int amplitude) {
        int amplitude1 = Math.max(1, Math.min(amplitude, mHeight / 2));
        int height = amplitude1 * 2;
        if (mWaveHeight != height) {
            mWaveHeight = height;
            updateMask(mWidth, mWaveLength, mWaveHeight);
            invalidate();
        }
    }

    /**
     * Set wave length (in pixels)
     * @param length
     */
    public void setWaveLength(int length) {
        int length1 = Math.max(8, Math.min(mWidth * 2, length));
        if (length1 != mWaveLength) {
            mWaveLength = length1;
            updateMask(mWidth, mWaveLength, mWaveHeight);
            invalidate();
        }
    }

    public void setLevel(int level) {
        setProgress(level / 10000f);
    }

    /**
     * Set the wave loading in indeterminate mode or not
     * @param indeterminate
     */
    public void setIndeterminate(boolean indeterminate) {
        mIndeterminate = indeterminate;
        if (mIndeterminate) {
            if (mAnimator == null) {
                mAnimator = getDefaultAnimator();
            }
            mAnimator.setValueUpdateListener(mAnimatorUpdateListener);
            mAnimator.start();
        } else {
            if (mAnimator != null) {
                mAnimator.setValueUpdateListener(null);
                mAnimator.cancel();
            }
            setLevel(calculateLevel());
        }
    }

    /**
     * Set customised animator for wave loading animation
     * @param animator
     */
    public void setIndeterminateAnimator(AnimatorValue animator) {
        if (mAnimator == animator) {
            return;
        }

        if (mAnimator != null) {
            mAnimator.setValueUpdateListener(null);
            mAnimator.cancel();
        }

        mAnimator = animator;
        if (mAnimator != null) {
            mAnimator.setValueUpdateListener(mAnimatorUpdateListener);
        }
    }

    private void setBounds(int left, int top, int right, int bottom) {
        Rect bound = new Rect(left, top, right, bottom);
        mDrawable.setBounds(bound);
    }

    private void updateBounds(Rect bounds) {
        if ((bounds.right-bounds.left) <= 0 || (bounds.bottom-bounds.top) <= 0) {
            return;
        }

        if (mWidth < 0 || mHeight < 0) {
            mWidth = bounds.right-bounds.left;
            mHeight = bounds.bottom-bounds.top;
            if (mWaveHeight == UNDEFINED_VALUE) {
                mWaveHeight = Math.max(8, (int) (mHeight * WAVE_HEIGHT_FACTOR));
            }

            if (mWaveLength == UNDEFINED_VALUE) {
                mWaveLength = mWidth;
            }

            if (mWaveStep == UNDEFINED_VALUE) {
                mWaveStep = Math.max(1, (int) (mWidth * WAVE_SPEED_FACTOR));
            }

            updateMask(mWidth, mWaveLength, mWaveHeight);
        }
    }

    public boolean isIndeterminate() {
        return mIndeterminate;
    }

    private AnimatorValue getDefaultAnimator() {
        AnimatorValue animatorValue = new AnimatorValue();
        animatorValue.setCurveType(Animator.CurveType.DECELERATE);
        animatorValue.setDelay(100);
        animatorValue.setLoopedCount(Animator.INFINITE);
        animatorValue.setDuration(5000);
        return animatorValue;
    }

//    @Override
//    public void setAlpha(float alpha) {
//        setAlpha(0.5f);
//    }

    private void setProgress(float progress) {
        mProgress = progress;
        mWaveLevel = mHeight - (int)((mHeight + mWaveHeight) * mProgress);
        invalidate();
    }

    private int calculateLevel() {
        return (mHeight - mWaveLevel) * 10000 / (mHeight + mWaveHeight);
    }



    private void updateMask(int width, int length, int height) {

        if (width <= 0 || length <= 0 || height <= 0) {
            mMask = null;
            return;
        }

        final int count = (int) Math.ceil((width + length) / (float)length);

        PixelMap.InitializationOptions options = new PixelMap.InitializationOptions();
        options.pixelFormat = PixelFormat.ARGB_8888;
        options.editable = true;
        options.size = new Size(length*count, height);
        PixelMap bm = PixelMap.create(options);


        Texture texture = new Texture(bm);
        //c.setTexture(texture);
        Canvas c = new Canvas(texture);

        Paint p = new Paint();
        p.setAntiAlias(true);
        p.setColor(Color.BLACK);

        int amplitude = height / 2;
        Path path = new Path();

        float x = 0;
        float y = -amplitude;
        path.moveTo(0, amplitude);

        final float stepX = length / 4f;

        for (int i = 0; i < count * 2; i++) {
            x = x + stepX;
            path.quadTo(x, y, x+stepX, amplitude);

            //x += stepX;
            x = x + stepX;
            y = bm.getImageInfo().size.height - y;

        }

        path.lineTo(options.size.width, height);
        path.lineTo(0, height);
        path.close();

        c.drawPath(path, p);

        mMask = bm;

    }

    public void draw() {
        addDrawTask(this);
    }

    @Override
    public void onDraw(Component component, Canvas canvas) {

        mDrawable.setColorMatrix(sGrayFilter);
        mDrawable.drawToCanvas(canvas);
        mDrawable.setColorMatrix(mCurFilter);

        if (mProgress <= 0.001f) {
            return;
        }

        int sc = canvas.saveLayer(new RectFloat(0,0,mWidth, mHeight), new Paint());

        if (mWaveLevel > 0) {
            canvas.clipRect(0, mWaveLevel, mWidth, mHeight);
        }

        mDrawable.drawToCanvas(canvas);


        if (mProgress >= 0.999f) {
            return;
        }

        mWaveOffset += mWaveStep;
        if (mWaveOffset > mWaveLength) {
            mWaveOffset -= mWaveLength;
        }

        if (mMask != null) {
            canvas.translate(-mWaveOffset, mWaveLevel);
            PixelMapHolder pixelMapHolder = new PixelMapHolder(mMask);
            canvas.drawPixelMapHolder(pixelMapHolder, 0, 0, mPaint);
        }

        canvas.restoreToCount(sc);
    }

    public void setColorFilter(ColorMatrix colorFilter) {
        mCurFilter = colorFilter;
        invalidate();
    }


    @Override
    public void start() {
        mRunning = true;
        postDoFrame();
    }

    private void postDoFrame() {

        dispatcher.asyncDispatch(new Runnable() {
            @Override
            public void run() {
                invalidate();
                if (mRunning) {
                    postDoFrame();
                }
            }
        });
    }


    @Override
    public void stop() {
        mRunning = false;
        //invalidate();
    }

    @Override
    public boolean isRunning() {
        return mRunning;
    }

    // 动画侦听函数
    private final AnimatorValue.ValueUpdateListener mAnimatorUpdateListener
            = new AnimatorValue.ValueUpdateListener() {
        @Override
        public void onUpdate(AnimatorValue animatorValue, float v) {
            if (mIndeterminate) {
                setProgress(v);
                if (!mRunning) {
                    invalidate();
                }
            }
        }
    };
}
