package com.race604.waveloading.slice;

import com.race604.drawable.wave.WaveLoadingView;
import com.race604.waveloading.ResourceTable;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;

import ohos.agp.components.RadioContainer;
import ohos.agp.components.Slider;
import ohos.agp.utils.Color;
import ohos.hiviewdfx.HiLog;
import ohos.hiviewdfx.HiLogLabel;


public class MainAbilitySlice extends AbilitySlice {
    static final HiLogLabel TAG = new HiLogLabel(HiLog.LOG_APP, 0, "chenyu");
    private WaveLoadingView mImageView;
    private Slider mLevelSeekBar;
    private Slider mAmplitudeSeekBar;
    private Slider mSpeedSeekBar;
    private Slider mLengthSeekBar;
    private RadioContainer mRadioGroup;

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_main);

        mImageView = (WaveLoadingView) findComponentById(ResourceTable.Id_image);
        mImageView.setWaveElement(this, ResourceTable.Media_robot);
        mImageView.draw();

        mLevelSeekBar = (Slider)findComponentById(ResourceTable.Id_level_seek);
        mLevelSeekBar.setValueChangedListener(new SimpleOnSeekBarChangeListener() {
            @Override
            public void onProgressUpdated(Slider slider, int progress, boolean fromUser) {
                mImageView.setLevel(progress);
            }
        });

        mAmplitudeSeekBar = (Slider)findComponentById(ResourceTable.Id_amplitude_seek);
        mAmplitudeSeekBar.setValueChangedListener(new SimpleOnSeekBarChangeListener() {
            @Override
            public void onProgressUpdated(Slider slider, int progress, boolean fromUser) {
                mImageView.setWaveAmplitude(progress);
            }
        });

        mLengthSeekBar = (Slider)findComponentById(ResourceTable.Id_length_seek);
        mLengthSeekBar.setValueChangedListener(new SimpleOnSeekBarChangeListener() {
            @Override
            public void onProgressUpdated(Slider slider, int progress, boolean fromUser) {
                mImageView.setWaveLength(progress);
            }
        });

        mSpeedSeekBar = (Slider)findComponentById(ResourceTable.Id_speed_seek);
        mSpeedSeekBar.setValueChangedListener(new SimpleOnSeekBarChangeListener() {
            @Override
            public void onProgressUpdated(Slider slider, int progress, boolean fromUser) {
                mImageView.setWaveSpeed(progress);
            }
        });

        mRadioGroup = (RadioContainer)findComponentById(ResourceTable.Id_modes);
        mRadioGroup.setMarkChangedListener(new RadioContainer.CheckedStateChangedListener() {
            @Override
            public void onCheckedChanged(RadioContainer radioContainer, int i) {
                final boolean indeterminate = i == 0;
                setIndeterminateMode(indeterminate);
            }
        });
        setIndeterminateMode(mRadioGroup.getMarkedButtonId() == 0);

        WaveLoadingView chromeWave = (WaveLoadingView)findComponentById(ResourceTable.Id_image2);
        chromeWave.setWaveElement(this, ResourceTable.Media_chrome_logo);
        chromeWave.draw();
        chromeWave.setIndeterminate(true);

        WaveLoadingView component = (WaveLoadingView) findComponentById(ResourceTable.Id_view);
        component.setWaveColor(Color.getIntColor("#FF4081"));
        component.draw();
        component.setIndeterminate(true);

    }

    private void setIndeterminateMode(boolean indeterminate) {
        mImageView.setIndeterminate(indeterminate);
        mLevelSeekBar.setEnabled(!indeterminate);

        if (!indeterminate) {
            mLevelSeekBar.setProgressColor(new Color(Color.getIntColor("#ff4084")));
        }
        else {
            mLevelSeekBar.setProgressColor(new Color(Color.getIntColor("#f0f0f0")));
        }

        if (!indeterminate) {
            mImageView.setLevel(mLevelSeekBar.getProgress());
        }
        mImageView.setWaveAmplitude(mAmplitudeSeekBar.getProgress());
        mImageView.setWaveLength(mLengthSeekBar.getProgress());
        mImageView.setWaveSpeed(mSpeedSeekBar.getProgress());
    }

    @Override
    public void onActive() {
        super.onActive();
    }

    @Override
    public void onForeground(Intent intent) {
        super.onForeground(intent);
    }


    private static class SimpleOnSeekBarChangeListener implements Slider.ValueChangedListener{

        @Override
        public void onProgressUpdated(Slider slider, int i, boolean b) {

        }

        @Override
        public void onTouchStart(Slider slider) {

        }

        @Override
        public void onTouchEnd(Slider slider) {

        }
    }
}
